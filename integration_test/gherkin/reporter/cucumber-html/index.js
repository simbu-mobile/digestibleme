const fs = require('fs');

let reporter = require('cucumber-html-reporter');
let metaData = fs.readFileSync('meta.json');
let meta = JSON.parse(metaData);

var options = {
        theme: 'bootstrap',
        jsonFile: 'feature_test_report.json',
        output: 'digestableme.html',
        reportSuiteAsScenarios: true,
        scenarioTimestamp: true,
        launchReport: true,
        metadata: meta
    };

    reporter.generate(options);
    

    //more info on `metadata` is available in `options` section below.

    //to generate consodilated report from multi-cucumber JSON files, please use `jsonDir` option instead of `jsonFile`. More info is available in `options` section below.