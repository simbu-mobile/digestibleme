// ignore_for_file: deprecated_member_use

import 'package:digestable_prologue/native_layout/tabs/provider/tab_items_provider.dart';
import 'package:digestable_prologue/prologue_app.dart';
import 'package:digestableme/app/data/tab_items.dart';
import 'package:digestableme/app/provider/mock_api_response_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/testing.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simbucore/app/core/model/application_mode.dart';
import 'package:simbucore/app/core/provider/application_mode_provider.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/app/core/service/global_environment_values.dart';
import 'package:simbucore/app/event_store/provider/event_provider.dart';
import 'package:simbucore/app/event_store/service/event_store.dart';
import 'package:simbucore/app/event_store/service/test_event_bus_service.dart';
import 'package:flutter_data/flutter_data.dart';
import 'package:http/http.dart' as http;

Future<void> runner() async {
  WidgetsFlutterBinding.ensureInitialized();
  GlobalEnvironmentValues.instance
      .loadValues(await rootBundle.loadString("app_config.json"));
  main();
}

// ignore: prefer_function_declarations_over_variables
Override Function({FutureFn<String>? baseDirFn, bool? clear, List<int>? encryptionKey}) configureRepositoryLocalStorage =
    ({FutureFn<String>? baseDirFn, List<int>? encryptionKey, bool? clear}) {
  if (!kIsWeb) {
    baseDirFn ??=
        () => getApplicationDocumentsDirectory().then((dir) => dir.path);
  } else {
    baseDirFn ??= () => '';
  }

  return hiveLocalStorageProvider
      .overrideWithProvider(Provider((ref) => HiveLocalStorage(
            hive: ref.read(hiveProvider),
            baseDirFn: baseDirFn,
            encryptionKey: encryptionKey,
            clear: clear ?? false ? LocalStorageClearStrategy.always : LocalStorageClearStrategy.never,
          )));
};

void main() {
  //IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  // Need to find a way to stop with error in IntegrationTestWidgetsFlutterBinding.tearDownAll await integrationTestChannel.invokeMethod<void>
  // MissingPluginException (MissingPluginException(No implementation found for method allTestsFinished on channel plugins.flutter.io/integration_test))

  runApp(
    ProviderScope(
      overrides: [
        configureRepositoryLocalStorage(),
        // Mock all http calls.
        // When we have environment API's, we will only run this for testing or developing without an API.
        httpClientProvider.overrideWithProvider(Provider((ref) {
          return MockClient((req) async {
            final response = ref.watch(mockAPIResponseProvider);
            final text = await response.callback(req);
            return http.Response(text, response.statusCode,
                headers: response.headers);
          });
        })),
        applicationModeProvider
            .overrideWithProvider(StateProvider<ApplicationMode>(
          (ref) => ApplicationMode.testing,
        )),
        eventStoreProvider.overrideWithProvider(Provider<EventStore>(
          (ref) => EventStore(TestEventBusService()),
        )),
        tabItemsProvider.overrideWithProvider(
          StateNotifierProvider((ref) => TabItemsNotifier(tabItems)),
        ),
      ],
      child: const PrologueApp(),
    ),
  );
}
