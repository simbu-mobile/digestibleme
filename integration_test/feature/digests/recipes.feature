Feature: Recipes
	Scenario: List Recipes 
		Given: The Digest tab is selected on home screen
		When: The recipes digest is chosen
		Then: A list of recipes is displayed
	Scenario: Show Recipe details
        Given: The recipe list is displayed
        When: A recipe is selected
        Then: The recipe details are shown
	# Scenario: Search for a Recipe
    #     Given: The Find Recipe screen
    #     When: I search for a recipe
	# 	Then: It displays the recipes found
	# Scenario: Add a Recipe
    #     Given: Recipe search results
    #     When: I tap on the "+" icon of a recipe
	# 	Then: It displays the recipes screen
	# 	And: It includes the newly added recipe