import 'package:gherkin/gherkin.dart';
import 'digests/recipes/recipe_steps.dart';
import 'app/tab_steps.dart';

Iterable<StepDefinitionGeneric> stepDefinitions = [
  ...recipeSteps(),
  ...tabSteps(),
];
