import 'package:digestableme/digests/recipes/widget/recipes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';
import 'package:simbucore/integration_test/gherkin/builder/extended_world_builder.dart';
import '../../app/tab_steps.dart';

Iterable<StepDefinitionGeneric> recipeSteps() => [
      givenTheRecipeListIsDisplayed(),
      givenTheFindRecipeScreen(),
      whenTheRecipeDisgestIsChosen(),
      whenARecipeIsSelected(),
      whenISearchForARecipe(),
      thenAListOfRecipiesIsDisplayed(),
      thenTheRecipeDetailsAreShow(),
      thenItDisplaysTheRecipesFound(),
    ];

StepDefinitionGeneric givenTheRecipeListIsDisplayed() {
  return given<ExtendedWorld>(
    'The recipe list is displayed',
    (context) async {
      await selectDigestTab(context);
      await selectRecipeDigest(context);
      await context.world.appDriver.waitForAppToSettle();
    },
  );
}

StepDefinitionGeneric givenTheFindRecipeScreen() {
  return given<ExtendedWorld>(
    'The Find Recipe screen',
    (context) async {
      
      final addRecipeButtonFinder = find.bySemanticsLabel("Add Recipe");
      await context.world.appDriver.tap(addRecipeButtonFinder.first);
      await context.world.appDriver.waitForAppToSettle();
    },
  );
}

StepDefinitionGeneric whenTheRecipeDisgestIsChosen() {
  return when<FlutterWorld>(
    'The recipes digest is chosen',
    (context) async {
      await selectRecipeDigest(context);
    },
  );
}

StepDefinitionGeneric whenARecipeIsSelected() {
  return when<FlutterWorld>(
    'A recipe is selected',
    (context) async {
      await context.world.appDriver.tap(find.text("How to boil an egg").first);
      await context.world.appDriver.waitForAppToSettle();
    },
  );
}

StepDefinitionGeneric whenISearchForARecipe() {
  return when<FlutterWorld>(
    'I search for a recipe',
    (context) async {
      final searchFieldFinder = find.byType(CupertinoSearchTextField);
      await context.world.appDriver.enterText(searchFieldFinder, "balsamic");
      await context.world.appDriver.enterText(searchFieldFinder, String.fromCharCode(13));
      //tester.testTextInput.receiveAction(TextInputAction.done)
      await simulateKeyDownEvent(LogicalKeyboardKey.enter);
      await context.world.appDriver.waitForAppToSettle();
    },
  );
}

StepDefinitionGeneric thenAListOfRecipiesIsDisplayed() {
  return then<FlutterWorld>(
    'A list of recipes is displayed',
    (context) async {
      var recipes =
          await context.world.appDriver.isPresent(find.byType(Recipes));
      context.expect(
        recipes,
        true,
      );
    },
  );
}

StepDefinitionGeneric thenTheRecipeDetailsAreShow() {
  return then<FlutterWorld>(
    'The recipe details are shown',
    (context) async {
      context.world.appDriver.waitUntil(
        () => context.world.appDriver.isPresent(find.text('saucepan')),
        pollInterval: const Duration(milliseconds: 200),
      );
    },
  );
}

StepDefinitionGeneric thenItDisplaysTheRecipesFound() {
  return then<FlutterWorld>(
    'It displays the recipes found',
    (context) async {
      context.world.appDriver.waitForAppToSettle();
      context.expect(
        1,
        true,
      );
      // context.world.appDriver.waitUntil(
      //   () => context.world.appDriver.isPresent(find.text('saucepan')),
      //   pollInterval: const Duration(milliseconds: 200),
      // );
    },
  );
}

Future<void> selectRecipeDigest(StepContext<FlutterWorld> context) async {
  // Nothing to do at the moment as recipes is the only digest.
}
