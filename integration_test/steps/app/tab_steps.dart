import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';

import 'package:simbucore/integration_test/gherkin/builder/extended_world_builder.dart';

Iterable<StepDefinitionGeneric> tabSteps() => [
  givenTheDigestTabIsSelected(),
];

StepDefinitionGeneric givenTheDigestTabIsSelected() {
  return given<ExtendedWorld>(
    'The Digest tab is selected on home screen',
    (context) async {
      await selectDigestTab(context);
    },
  );
}

Future<void> selectDigestTab(StepContext<ExtendedWorld> context) async {
  if (context.world.isAndroid){
    var hamburgerMenu = find.byTooltip('Open navigation menu').first;
    await context.world.appDriver.tap(hamburgerMenu);
    await context.world.appDriver.waitForAppToSettle();
  }
  await context.world.appDriver.tap(find.text("Digests").first);
  await context.world.appDriver.waitForAppToSettle();
}