
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gherkin/gherkin.dart';

// The application under test.
import 'app_runner.dart' as app;

// Feature steps
import 'package:simbucore/integration_test/gherkin/builder/config_builder.dart';

part 'gherkin_feature_test.g.dart';

const String featureUnderTest = "feature/digests/recipes.feature";

Iterable<StepDefinitionGeneric> stepDefinitions = [
];

@GherkinTestSuite(featurePaths: ['integration_test/$featureUnderTest',])
void main() {
  final testConfig = gherkinTestConfiguration([RegExp(featureUnderTest),], stepDefinitions);

  executeTestSuite(configuration: testConfig, appMainFunction: (World world) => app.runner());
}