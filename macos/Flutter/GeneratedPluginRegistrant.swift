//
//  Generated file. Do not edit.
//

import FlutterMacOS
import Foundation

import azure_ad_authentication
import connectivity_plus
import device_info_plus
import path_provider_foundation

func RegisterGeneratedPlugins(registry: FlutterPluginRegistry) {
  AzureAdAuthenticationPlugin.register(with: registry.registrar(forPlugin: "AzureAdAuthenticationPlugin"))
  ConnectivityPlugin.register(with: registry.registrar(forPlugin: "ConnectivityPlugin"))
  DeviceInfoPlusMacosPlugin.register(with: registry.registrar(forPlugin: "DeviceInfoPlusMacosPlugin"))
  PathProviderPlugin.register(with: registry.registrar(forPlugin: "PathProviderPlugin"))
}
