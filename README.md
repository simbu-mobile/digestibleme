# DigestableMe

A sharable collection of things of interest, music, food, travel, sport.

As it grows parts of the project will be moved into packages that are not hosted on pub.io:

## Packages

[DigestableLego](https://gitlab.com/simbu-mobile/digestablelego) - UI widgets e.g DmList for displaying list items.

## Blog

You can find a series of articles about this project at [Medium](https://medium.com/@simbu).

## As Is

Except as represented in this agreement, all work product by Developer is provided ​“AS IS”. Other than as provided in this agreement, Developer makes no other warranties, express or implied, and hereby disclaims all implied warranties, including any warranty of merchantability and warranty of fitness for a particular purpose.

## Flutter

Resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Known Issues

### Feature test with cumcumber report broken after project restructure

``` code
flutter drive -driver=test_driver/integration_test_feature_driver.dart --target=integration_test/gherkin_suite_test.dart -d 0A2BDB91-D9C7-4897-94EA-C48A499CCE19
```

is failing with unusual message

```Test file not found: /Users/simbu/Src/flutter/digestableme/test_driver/gherkin_suite_test_test.dart```

But it appears that the issue actually lies in the flutter_gherkin cmd to the run the tests, so will need to put it back together again piece by piece

![Integration Test Error](readme_assets\broken_integration_tests.png)

### Exception thrown after Integration tests finish, missing allTestsFinished method on channel plugins.flutter.io/integration_test

IntegrationTestWidgetsFlutterBinding.tearDownAll calls integrationTestChannel.invokeMethod 'allTestsFinished' that does not exist
Not sure how to implement it to avoid the error, it causes a timeout at the end of the integration test run when debugging:

``` code
flutter: ══╡ EXCEPTION CAUGHT BY FLUTTER TEST FRAMEWORK ╞════════════════════════════════════════════════════
flutter: The following TimeoutException was thrown running a test (but after the test had completed):
flutter: TimeoutException after 0:00:10.000000: Future not completed
flutter:
flutter: When the exception was thrown, this was the stack
flutter: ════════════════════════════════════════════════════════════════════════════════════════════════════
```

When the error is fixed it will return control and we should see the test finished screen.

![Integration Test Finished](readme_assets\integration_test_finished.png)
