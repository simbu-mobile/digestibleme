// Flutter imports:
// ignore_for_file: deprecated_member_use

import 'package:digestable_prologue/native_layout/tabs/provider/tab_items_provider.dart';
import 'package:digestable_prologue/prologue_app.dart';
import 'package:digestable_prologue/security/model/Authenication.dart';
import 'package:digestable_prologue/security/model/authentication_mode.dart';
import 'package:digestable_prologue/security/provider/authentication_provider.dart';
import 'package:digestableme/app/provider/mock_api_response_provider.dart';
import 'package:digestableme/main.data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_data/flutter_data.dart';

// Package imports:
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/app/core/model/environments.dart';
import 'package:simbucore/app/core/service/global_environment_values.dart';

import 'app/data/tab_items.dart';
import 'package:http/http.dart' as http;
import 'package:http/testing.dart';

/*
#######                              ######                        
#       #    # ##### #####  #   #    #     #  ####  # #    # ##### 
#       ##   #   #   #    #  # #     #     # #    # # ##   #   #   
#####   # #  #   #   #    #   #      ######  #    # # # #  #   #   
#       #  # #   #   #####    #      #       #    # # #  # #   #   
#       #   ##   #   #   #    #      #       #    # # #   ##   #   
####### #    #   #   #    #   #      #        ####  # #    #   #   
*/

/// Digestable Me application, A sharable collection of things of interest, music, food, travel, sport.
void main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  GlobalEnvironmentValues.instance
      .loadValues(await rootBundle.loadString("app_config_secret.json"));
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  var overrides = [
    // Create local DB Hive files if they don't exist.
    configureRepositoryLocalStorage(),
    tabItemsProvider.overrideWithProvider(
      StateNotifierProvider((ref) => TabItemsNotifier(tabItems)),
    ),
  ];
  if (GlobalEnvironmentValues.instance.environment != Environments.live) {
    overrides.add(
      // Mock all http calls.
      // When we have environment API's, we will only run this for testing or developing without an API.
      httpClientProvider.overrideWithProvider(
        Provider(
          (ref) {
            return MockClient((req) async {
              final response = ref.watch(mockAPIResponseProvider);
              final text = await response.callback(req);
              return http.Response(text, response.statusCode,
                  headers: response.headers);
            });
          },
        ),
      ),
    );
    overrides.add(authenticationProvider.overrideWithProvider(
      StateNotifierProvider(
        (ref) => AuthenticationStateNotifier(
            Authentication(AuthenticationMode.alwaysOn, "Joe", "")),
      ),
    ));
  }
  runApp(
    ProviderScope(
      overrides: overrides,
      child: const PrologueApp(),
    ),
  );
}
