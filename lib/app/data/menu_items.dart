// Project imports:
import 'package:digestable_prologue/native_layout/sidedraw/model/menu_item.dart';
import 'package:digestableme/app/data/tab_items.dart';

// Use to display a side draw menu.
List<MenuItemVM> get menuItems {
  var items = <MenuItemVM>[];
  for (var tabItem in tabItems) {
    items.add(MenuItemVM(tabItem.icon, tabItem.label ?? ""));
  }
  return items;
}