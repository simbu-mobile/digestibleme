
import 'dart:async';

import 'package:digestable_prologue/security/service/msal_authentication_service.dart'
    if (dart.library.js) 'package:digestable_prologue/security/service/msal_js_authentication_service.dart';
import 'package:flutter_data/flutter_data.dart';
import 'package:simbucore/app/core/service/global_environment_values.dart';

mixin JsonServerAdapter<T extends DataModel<T>> on RemoteAdapter<T> {
  @override
  Future<Map<String, String>> get defaultHeaders async {
    var msalService = getAuthenticationService();
    var authResult = await msalService.signIn();
    var bearerToken = authResult.accessToken;

    // Return a map containing the bearer token in the Authorization header
    return {'Authorization': 'Bearer $bearerToken'};
  }

  @override
  String get baseUrl => GlobalEnvironmentValues.instance.apiEndpoint;
}