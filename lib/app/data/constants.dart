/*
  For simplicity, I'm putting all constants like strings here for now, can change to resources etc and localisation when required.
*/

/// Application constants e.g. title
class ApplicationConstant{
  static const String title = "DigestableMe";
  static const String headerLabel = "Digestable Me";
  static const String recipeDigestTitle = "Recipes";
}