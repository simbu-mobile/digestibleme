import 'package:http/http.dart' as http;
import 'package:http/http.dart';

const recipies = '''
[
  {
    "id":1,
    "ingredients":[
      {
        "id":1,
        "quantity": 1,
        "quantityType": "unit",
        "name": "egg",
        "type": "Dairy"
      }
    ],
    "steps":[
      {
        "id":1,
        "sequenceNumber": 1,
        "instruction": "Take a small saucepan with a glass lid and carefully place a single egg (or two, or three) inside it. Burford brown eggs have a nice orange yolk. Fill the pan so the water only just covers the eggs, not even a millimetre more. If you had a centimetre of water covering the egg then you could still get the same result, but you would have to play with the timing."
      },
      {
        "id":2,
        "sequenceNumber": 2,
        "instruction": "Put the pan on maximum heat with the lid on and bring to the boil."
      },
      {
        "id":3,
        "sequenceNumber": 3,
        "instruction": "As soon as the water starts to bubble, remove from the heat. As you take the pan off, set a timer for six minutes; keep the lid on. Make sure you time it exactly, and you will end up with the perfect egg."
      }
    ],
    "title":"How to boil an egg",
    "link":"https://www.theguardian.com/lifeandstyle/2014/nov/11/how-to-boil-an-egg-the-heston-blumenthal-way",
    "source":"Heston Blumenthal",
    "thumbnail":"https://i.guim.co.uk/img/static/sys-images/Guardian/Pix/pictures/2014/11/5/1415205733799/4bfbd71a-6cd0-4494-833f-eaaed20a15b3-1020x612.jpeg?width=620&quality=45&auto=format&fit=max&dpr=2&s=ca3a95d7e761d267eff1b79b58cc4849",
    "totalTime":""
  }
]
''';
/* Addding another recipe
,
  {
    "id": 2,
    "ingredients":[],
    "steps":[],
    "title":"Prune Juice",
    "link":"https://www.theguardian.com/lifeandstyle/2014/nov/11/how-to-boil-an-egg-the-heston-blumenthal-way",
    "source":"Piggy Pie",
    "thumbnail":"https://i.guim.co.uk/img/static/sys-images/Guardian/Pix/pictures/2014/11/5/1415205733799/4bfbd71a-6cd0-4494-833f-eaaed20a15b3-1020x612.jpeg?width=620&quality=45&auto=format&fit=max&dpr=2&s=ca3a95d7e761d267eff1b79b58cc4849",
    "totalTime":""
  }
*/

//https://serpapi.com/search.json?q=Roast+potatoes+with+balsamic&location=United+Kingdom&hl=en&gl=uk&google_domain=google.co.uk&api_key=secret_api_key
const serpApiRoastPotatoesWithBalsamic = '''
  {
    "search_metadata": {
      "id": "61c3369cc99903747c1e643b",
      "status": "Success",
      "json_endpoint": "https://serpapi.com/searches/bdb5db8a7f4c5593/61c3369cc99903747c1e643b.json",
      "created_at": "2021-12-22 14:30:52 UTC",
      "processed_at": "2021-12-22 14:30:52 UTC",
      "google_url": "https://www.google.co.uk/search?q=Roast+potatoes+with+balsamic&oq=Roast+potatoes+with+balsamic&uule=w+CAIQICIOVW5pdGVkIEtpbmdkb20&hl=en&gl=uk&sourceid=chrome&ie=UTF-8",
      "raw_html_file": "https://serpapi.com/searches/bdb5db8a7f4c5593/61c3369cc99903747c1e643b.html",
      "total_time_taken": 1.99
    },
    "search_parameters": {
      "engine": "google",
      "q": "Roast potatoes with balsamic",
      "location_requested": "United Kingdom",
      "location_used": "United Kingdom",
      "google_domain": "google.co.uk",
      "hl": "en",
      "gl": "uk",
      "device": "desktop"
    },
    "search_information": {
      "organic_results_state": "Results for exact spelling",
      "query_displayed": "Roast potatoes with balsamic",
      "total_results": 12800000,
      "time_taken_displayed": 0.49
    },
    "recipes_results": [
      {
        "title": "Balsamic potatoes",
        "link": "https://www.jamieoliver.com/recipes/potato-recipes/balsamic-potatoes/",
        "source": "Jamie Oliver",
        "total_time": "2 hrs 20 mins",
        "ingredients":
        [
          "Balsamic vinegar",
          "maris piper potatoes",
          "red onions",
          "rocket",
          "olive oil"
        ],
        "thumbnail": "assets/balsamicpotatoes_jamie.jpg"
      },
      {
        "title": "Balsamic roast potatoes",
        "link": "https://www.taste.com.au/recipes/balsamic-roast-potatoes/1bcb6bd4-2efa-4f01-bc98-f2ce32eaa906",
        "source": "Taste",
        "rating": 4,
        "reviews": 2,
        "total_time": "1 hr 5 mins",
        "ingredients":
        [
          "Balsamic vinegar",
          "kipfler potatoes",
          "olive oil",
          "garlic"
        ],
        "thumbnail": "assets/balsamicpotatoes_taste.jpg"
      }
    ]
  }
  ''';

class MockedResponse {
  final Future<String> Function(http.Request) callback;
  final int statusCode;
  final Map<String, String> headers;

  const MockedResponse(
    this.callback, {
    this.statusCode = 200,
    this.headers = const {},
  });

  // Return response as function (Future), we are just mimicking how an API works so we can be switched in/out.
  factory MockedResponse.text(String requestUrl) => MockedResponse(
        (_) async => lookupResponse(_),
        statusCode: 200,   
        headers: const {},
      );

  // Find the mock response using the request address.
  //final recipesData = await ApiClient().get("/recipe");
    //var recipes = Recipe.fromResultJson(recipesData.data["Recipies"]).toList();
  static String lookupResponse(Request request) {
    var requestPath = request.url.path;
    var jsonString = responses[requestPath] ?? "{}";
    return jsonString;
  }

  // Simple lookup that returns JSON responses.
  // Lookup: key = request url, value = json response.
  static const Map<String, String> responses = <String, String>{
    '/digests/recipes': recipies,
    '/recipe/search?q=Roast+potatoes+with+balsamic':
        serpApiRoastPotatoesWithBalsamic
  };
}
