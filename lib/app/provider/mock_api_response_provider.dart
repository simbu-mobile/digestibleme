import 'package:digestableme/app/data/mock_api.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final mockAPIResponseProvider =
    StateProvider<MockedResponse>((_) => MockedResponse.text(''));