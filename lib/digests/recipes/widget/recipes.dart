// Flutter imports:
import 'package:digestableme/main.data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:

import 'package:layout_lego/models/dl_list_item.dart';
import 'package:layout_lego/dl_list.dart';
import 'package:asset_lego/dl_avatar.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:universal_platform/universal_platform.dart';

// Project imports:
import 'package:simbucore/app/core/extension/image_extension.dart';
import 'package:simbucore/app/core/extension/widget_extension.dart';
import 'package:simbucore/app/core/provider/application_mode_provider.dart';
import 'package:simbucore/app/core/provider/platform_provider.dart';
import 'package:digestableme/digests/recipes/model/recipe.dart';
import 'package:digestableme/digests/recipes/provider/recipe_provider.dart';
import 'package:digestableme/digests/recipes/widget/recipe_details.dart';

class Recipes extends ConsumerWidget {
  const Recipes({Key? key}) : super(key: key);

  iosNavigateToSelectedRecipe(BuildContext context) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const CupertinoPageScaffold(
          navigationBar: CupertinoNavigationBar(
            heroTag: "RecipeDetailsHeroTag",
            transitionBetweenRoutes: false,
          ),
          child: RecipeDetails(),
        ),
      ),
    );
  }

  androidNavigateToSelectedRecipe(
      BuildContext context, UniversalPlatformType platformType) {
    return const SafeArea(child: Scaffold(body: RecipeDetails()))
        .pushRouteNavigation(context, platformType);
  }
 
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(repositoryInitializerProvider).when(
          error: (error, _) => Text(error.toString()),
          loading: () => const CircularProgressIndicator(),
          data: (_) {
            final state = ref.recipes.watchAll();
            if (state.isLoading) {
              return const Center(child: CircularProgressIndicator());
            } 
            return (List<Recipe> recipes, BuildContext context, WidgetRef ref) {
              List<DlListItem> listItems = [];
              for (var recipe in recipes) {
                listItems.add(DlListItem(
                  leading: DlAvatar(
                    context.networkImage(
                        recipe.thumbnail, ref.watch(applicationModeProvider)),
                  ),
                  title: Text(recipe.title),
                  subtitle: Text(recipe.author),
                  onTap: () {
                    ref
                        .read(selectedRecipeProvider.notifier)
                        .update((state) => recipe);
                    if (ref.read(platformProvider) ==
                        UniversalPlatformType.IOS) {
                      iosNavigateToSelectedRecipe(context);
                    }
                    if (ref.read(platformProvider) ==
                        UniversalPlatformType.Android) {
                      androidNavigateToSelectedRecipe(
                          context, ref.watch(platformProvider));
                    }
                  },
                ));
              }
 
              return DlList(listItems);
            }(
              state.model ?? [],
              context,
              ref,
            );
          },
        );
  }
}
