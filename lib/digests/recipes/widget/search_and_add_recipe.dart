// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:layout_lego/models/dl_list_item.dart';
import 'package:layout_lego/dl_scrollable.dart';
import 'package:layout_lego/dl_list_tile.dart';
import 'package:asset_lego/dl_avatar.dart';
import 'package:dio/dio.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

// Project imports:
import 'package:digestableme/app/data/api_client.dart';
import 'package:digestableme/digests/recipes/model/recipe.dart';
import 'package:digestableme/digests/recipes/widget/search_bar.dart';

class SearchAndAddRecipe extends ConsumerStatefulWidget {
  const SearchAndAddRecipe({Key? key}) : super(key: key);

  @override
  ConsumerState<SearchAndAddRecipe> createState() => _SearchAndAddRecipeState();
}

class _SearchAndAddRecipeState extends ConsumerState<SearchAndAddRecipe> {
  final ApiClient _api = ApiClient();
  late TextEditingController _textController;
  late Response<dynamic> _searchResults;

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController(text: '');
    _searchResults = _api.emptyResult();
  }

  @override
  Widget build(BuildContext context) {
    //Recipe.fromSerpApiResultJson(apiClient.get("/recipe/search?q=Roast+potatoes+with+balsamic").data);
    var searchResultRecipes = _searchResults.data.length > 0
        ? Recipe.fromSerpApiResultJson(_searchResults.data)
        : <Recipe>[];

    return CupertinoPageScaffold(
      navigationBar: navBar(),
      child: DlScrollable(items: [
        buildSearch(),
        for (var result in searchResultRecipes) searchResult(result)
      ]),
    );
  }

  Widget searchResult(Recipe result) {
    var listItem = DlListItem(
            leading: DlAvatar(
              Image(image: AssetImage(result.thumbnail)),
            ),
            title: Text(result.title),
            subtitle: Text(result.author),
            onTap: () {},
            trailing: const Icon(CupertinoIcons.add_circled_solid)
          );

    return DlListTile(listItem);
  }

  CupertinoNavigationBar navBar() {
    return const CupertinoNavigationBar(
      heroTag: "RecipeSearchAndAddHeroTag",
      transitionBetweenRoutes: false,
      middle: Text(
        "Find Recipe",
      ),
    );
  }

  Stack buildSearch() {
    return Stack(
      children: [
        Material(
          elevation: 27,
          child: Image.asset(
            "assets/recipes_sm3.jpg",
            fit: BoxFit.fill,
          ),
        ),
        SearchBar(
          _textController,
          (String value) {
            if (value.toLowerCase().contains("roast") ||
                value.toLowerCase().contains("potatoe") ||
                value.toLowerCase().contains("balsamic")) {
              setState(() {
                _api.get("/recipe/search?q=Roast+potatoes+with+balsamic").then((searchResults) => _searchResults = searchResults);
              });
            } else {
              setState(() {
                _searchResults = _api.emptyResult();
              });
            }
          },
        )
        //buildSearchField(),
      ],
    );
  }
}
