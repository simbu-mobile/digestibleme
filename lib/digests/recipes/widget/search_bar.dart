// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/theme/provider/theme_provider.dart';

// Stateful because of the textEditingController, was getting an error when stateless.
class SearchBar extends ConsumerStatefulWidget {
  final TextEditingController textEditingController;
  final ValueChanged<String>? onSubmitted;
  const SearchBar(this.textEditingController, this.onSubmitted, {Key? key})
      : super(key: key);

  @override
  ConsumerState<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends ConsumerState<SearchBar> {
  @override
  Widget build(BuildContext context) {
    var themer = ref.watch(themeProvider);
    return Padding(
      padding: const EdgeInsets.all(15),
      child: CupertinoSearchTextField(
        backgroundColor: themer.brightness == Brightness.dark ? CupertinoColors.darkBackgroundGray : CupertinoColors.extraLightBackgroundGray,
        style: themer.typo.body,
        controller: widget.textEditingController,
        placeholder: "Recipes...", 
        onSubmitted: widget.onSubmitted,
      ),
    );
  }
}
