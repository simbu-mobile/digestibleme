// Flutter imports:
import 'package:digestableme/digests/recipes/widget/recipes.dart';
import 'package:digestableme/digests/recipes/widget/search_and_add_recipe.dart';
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/theme/provider/theme_provider.dart';

class IosDigestsTab extends ConsumerWidget {
  const IosDigestsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        heroTag: "RecipeTabHeroTag",
        transitionBetweenRoutes: false,
        middle: Text(
          "Recipes",
          style: ref.watch(themeProvider).typo.navBar,
        ),
        trailing: GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              CupertinoPageRoute(
                builder: (context) => const SearchAndAddRecipe(),
              ),
            );
          },
          child: const Icon(CupertinoIcons.add, semanticLabel: "Add Recipe",),
        ),
      ),
      child: const Recipes(),
    );
  }
}
