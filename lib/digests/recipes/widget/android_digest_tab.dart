// Flutter imports:
import 'package:digestableme/digests/recipes/widget/recipes.dart';
import 'package:flutter/material.dart';

class AndroidDigestTab extends StatelessWidget {
  const AndroidDigestTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Recipes();
  }
}
