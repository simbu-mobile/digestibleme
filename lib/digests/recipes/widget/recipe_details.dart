/*
  Recipe details

  The layout is loosely based on https://www.etsy.com/uk/listing/1086273678/editable-recipe-card-template-recipe?gpla=1&gao=1&&utm_source=google&utm_medium=cpc&utm_campaign=shopping_uk_en_gb_d-paper_and_party_supplies-other&utm_custom1=_k_Cj0KCQiAip-PBhDVARIsAPP2xc19gpUnxE45VTqUeNgOLs6tWI8pbAU86tI3LYdhU-YM0Zf7Fr29I8oaApZzEALw_wcB_k_&utm_content=go_12576528914_120381161035_507694609097_pla-314261241267_c__1086273678engb_102858184&utm_custom2=12576528914&gclid=Cj0KCQiAip-PBhDVARIsAPP2xc19gpUnxE45VTqUeNgOLs6tWI8pbAU86tI3LYdhU-YM0Zf7Fr29I8oaApZzEALw_wcB
 */

// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:layout_lego/dl_scrollable.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/app/core/model/application_mode.dart';
import 'package:simbucore/theme/provider/theme_provider.dart';
import 'package:simbucore/theme/themer.dart';

// Project imports:
import 'package:simbucore/app/core/extension/image_extension.dart';
import 'package:simbucore/app/core/provider/application_mode_provider.dart';
import 'package:digestableme/digests/recipes/model/recipe.dart';
import 'package:digestableme/digests/recipes/provider/recipe_provider.dart';

class RecipeDetails extends ConsumerWidget {
  const RecipeDetails({Key? key}) : super(key: key);

  static const String ingredientsTitle = "Ingredients";
  static const String stepsTitle = "Steps";

  Padding recipeTitleAndAuthor(BuildContext context, Themer themer, Recipe? recipe) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, bottom: 8),
      child: Center(
          child: Column(
        children: [
          Text(recipe!.title, style: themer.typo.title3),
          Text(recipe.author, style: themer.typo.body),
        ],
      )),
    );
  }

  Widget recipeImage(
    BuildContext context,
    Recipe? recipe,
    ApplicationMode mode
  ) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: context.networkImage(
        recipe!.thumbnail,
        mode,
        fit: BoxFit.fill,
      ),
    );
  }

  List<Widget> toDisplayList(
    BuildContext context,
    Themer themer,
    String title,
    Recipe? recipe,
  ) {
  
    Iterable<String> items = title == RecipeDetails.ingredientsTitle
        ? recipe!.ingredients.map((i) => i.toString())
        : recipe!.steps.map((i) => i.toString());

    return <Widget>[
      Visibility(
        visible: title != "",
        child: Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: Center(
              child: Text(
            title,
            style: themer.typo.title3,
          )),
        ),
      ),
      for (var item in items)
        Padding(
          padding: const EdgeInsets.only(
            left: 34,
            right: 34,
            top: 10,
          ),
          child: Center(
            child: Text(
              item,
              textAlign: TextAlign.justify,
            ),
          ),
        )
    ];
  }

  List<Widget> ingredients(BuildContext context, Themer themer, Recipe? recipe) {
    return toDisplayList(
      context,
      themer,
      RecipeDetails.ingredientsTitle,
      recipe,
    );
  }

  List<Widget> instructions(BuildContext context, Themer themer, Recipe? recipe) {
    return toDisplayList(
      context,
      themer,
      "", //stepsTitle,
      recipe,
    );
  }

  Padding shrug() {
    return const Padding(
      padding: EdgeInsets.only(top: 10.0, bottom: 40),
      child: Center(
          child: Text(
        " ¯¯\\_(ツ)_/¯¯",
        style: TextStyle(fontSize: 35),
      )),
    );
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var recipe = ref.watch(selectedRecipeProvider);
    if (recipe == null) {
      return Container();
    }

    var themer = ref.watch(themeProvider);

    return SafeArea(
      child: DlScrollable(items: <Widget>[
        recipeTitleAndAuthor(context, themer, recipe),
        recipeImage(context, recipe, ref.watch(applicationModeProvider)),
        for (Widget item in ingredients(context, themer, recipe,)) item,
        for (Widget item in instructions(context, themer, recipe,)) item,
        shrug()
      ]),
    );
  }
}
