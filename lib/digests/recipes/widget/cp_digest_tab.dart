// Flutter imports:
import 'package:digestableme/digests/recipes/widget/android_digest_tab.dart';
import 'package:digestableme/digests/recipes/widget/ios_digests_tab.dart';
import 'package:flutter/widgets.dart';

// Package imports:
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:universal_platform/universal_platform.dart';

// Project imports:
import 'package:simbucore/app/core/provider/platform_provider.dart';

class CpDigestTab extends ConsumerWidget {
  const   CpDigestTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(platformProvider) == UniversalPlatformType.IOS
        ? const IosDigestsTab()
        : const AndroidDigestTab();
  }
}
