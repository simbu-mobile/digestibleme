// Package imports:
import 'package:hooks_riverpod/hooks_riverpod.dart';

// Project imports:
import 'package:digestableme/app/data/api_client.dart';
import 'package:digestableme/digests/recipes/model/recipe.dart';

final recipesProvider = FutureProvider.autoDispose<List<Recipe>>(
  (ref) async {
    final recipesData = await ApiClient().get("/digests/recipes");
    var recipes = Recipe.fromResultJson(recipesData.data["Recipies"]).toList();
    return recipes;
  }
);

final selectedRecipeProvider = StateProvider<Recipe?>(
  (ref) => null
);
