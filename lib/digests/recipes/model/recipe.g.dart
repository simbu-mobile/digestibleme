// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipe.dart';

// **************************************************************************
// RepositoryGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, duplicate_ignore

mixin $RecipeLocalAdapter on LocalAdapter<Recipe> {
  static final Map<String, RelationshipMeta> _kRecipeRelationshipMetas = {
    'ingredients': RelationshipMeta<Ingredient>(
      name: 'ingredients',
      type: 'ingredients',
      kind: 'HasMany',
      instance: (_) => (_ as Recipe).ingredients,
    ),
    'steps': RelationshipMeta<Step>(
      name: 'steps',
      type: 'steps',
      kind: 'HasMany',
      instance: (_) => (_ as Recipe).steps,
    )
  };

  @override
  Map<String, RelationshipMeta> get relationshipMetas =>
      _kRecipeRelationshipMetas;

  @override
  Recipe deserialize(map) {
    map = transformDeserialize(map);
    return _$RecipeFromJson(map);
  }

  @override
  Map<String, dynamic> serialize(model, {bool withRelationships = true}) {
    final map = _$RecipeToJson(model);
    return transformSerialize(map, withRelationships: withRelationships);
  }
}

final _recipesFinders = <String, dynamic>{};

// ignore: must_be_immutable
class $RecipeHiveLocalAdapter = HiveLocalAdapter<Recipe>
    with $RecipeLocalAdapter;

class $RecipeRemoteAdapter = RemoteAdapter<Recipe>
    with JsonServerAdapter<Recipe>;

final internalRecipesRemoteAdapterProvider = Provider<RemoteAdapter<Recipe>>(
    (ref) => $RecipeRemoteAdapter(
        $RecipeHiveLocalAdapter(ref), InternalHolder(_recipesFinders)));

final recipesRepositoryProvider =
    Provider<Repository<Recipe>>((ref) => Repository<Recipe>(ref));

extension RecipeDataRepositoryX on Repository<Recipe> {
  JsonServerAdapter<Recipe> get jsonServerAdapter =>
      remoteAdapter as JsonServerAdapter<Recipe>;
}

extension RecipeRelationshipGraphNodeX on RelationshipGraphNode<Recipe> {
  RelationshipGraphNode<Ingredient> get ingredients {
    final meta = $RecipeLocalAdapter._kRecipeRelationshipMetas['ingredients']
        as RelationshipMeta<Ingredient>;
    return meta.clone(
        parent: this is RelationshipMeta ? this as RelationshipMeta : null);
  }

  RelationshipGraphNode<Step> get steps {
    final meta = $RecipeLocalAdapter._kRecipeRelationshipMetas['steps']
        as RelationshipMeta<Step>;
    return meta.clone(
        parent: this is RelationshipMeta ? this as RelationshipMeta : null);
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Recipe _$RecipeFromJson(Map<String, dynamic> json) => Recipe(
      id: json['id'] as int?,
      title: json['title'] as String,
      recipeLink: json['recipeLink'] as String,
      author: json['author'] as String,
      duration: json['duration'] as String,
      ingredients: HasMany<Ingredient>.fromJson(
          json['ingredients'] as Map<String, dynamic>),
      steps: HasMany<Step>.fromJson(json['steps'] as Map<String, dynamic>),
      thumbnail: json['thumbnail'] as String,
    );

Map<String, dynamic> _$RecipeToJson(Recipe instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'recipeLink': instance.recipeLink,
      'author': instance.author,
      'duration': instance.duration,
      'ingredients': instance.ingredients,
      'steps': instance.steps,
      'thumbnail': instance.thumbnail,
    };
