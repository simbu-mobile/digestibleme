// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'step.dart';

// **************************************************************************
// RepositoryGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, duplicate_ignore

mixin $StepLocalAdapter on LocalAdapter<Step> {
  static final Map<String, RelationshipMeta> _kStepRelationshipMetas = {};

  @override
  Map<String, RelationshipMeta> get relationshipMetas =>
      _kStepRelationshipMetas;

  @override
  Step deserialize(map) {
    map = transformDeserialize(map);
    return _$StepFromJson(map);
  }

  @override
  Map<String, dynamic> serialize(model, {bool withRelationships = true}) {
    final map = _$StepToJson(model);
    return transformSerialize(map, withRelationships: withRelationships);
  }
}

final _stepsFinders = <String, dynamic>{};

// ignore: must_be_immutable
class $StepHiveLocalAdapter = HiveLocalAdapter<Step> with $StepLocalAdapter;

class $StepRemoteAdapter = RemoteAdapter<Step> with NothingMixin;

final internalStepsRemoteAdapterProvider = Provider<RemoteAdapter<Step>>(
    (ref) => $StepRemoteAdapter(
        $StepHiveLocalAdapter(ref), InternalHolder(_stepsFinders)));

final stepsRepositoryProvider =
    Provider<Repository<Step>>((ref) => Repository<Step>(ref));

extension StepDataRepositoryX on Repository<Step> {}

extension StepRelationshipGraphNodeX on RelationshipGraphNode<Step> {}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Step _$StepFromJson(Map<String, dynamic> json) => Step(
      id: json['id'] as int?,
      sequenceNumber: json['sequenceNumber'] as int,
      instruction: json['instruction'] as String,
    );

Map<String, dynamic> _$StepToJson(Step instance) => <String, dynamic>{
      'id': instance.id,
      'sequenceNumber': instance.sequenceNumber,
      'instruction': instance.instruction,
    };
