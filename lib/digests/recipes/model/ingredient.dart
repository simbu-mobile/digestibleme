/* 
  A recipe ingredient, a recipe can have many e.g.
    "quantity": "1",
    "quantityType": "unit",
    "name": "egg",
    "type": "Dairy"
*/

import 'package:flutter_data/flutter_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ingredient.g.dart';

@JsonSerializable()
@DataRepository([])
class Ingredient extends DataModel<Ingredient>{
  @override
  final int? id;
  final int? quantity;
  final String? quantityType;
  final String name;
  final String? ingredientType;

  Ingredient({
    this.id,
    required this.name,
    this.quantityType,
    this.ingredientType,
    this.quantity,
  });

  @override
  String toString() {
    var qty = quantity == null ? "" : "$quantity ";
    return qty + name;
  }
}