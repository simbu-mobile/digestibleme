/* 
  A recipe step, a recipe can have many e.g.
     "sequenceNumber": "2",
              "instruction": "Put the pan on maximum heat with the lid on and bring to the boil."
*/

import 'package:flutter_data/flutter_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'step.g.dart';

@JsonSerializable()
@DataRepository([])
class Step extends DataModel<Step>{
  @override
  final int? id;
  final int sequenceNumber;
  final String instruction;

  Step({
    this.id,
    required this.sequenceNumber,
    required this.instruction,
  });

  @override
  String toString() {
    //return sequenceNumber.toString() + ". " + instruction;
    return instruction;
  }
}
