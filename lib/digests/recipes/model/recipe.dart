/*
  Recipe

    "title": "Balsamic potatoes",
    "link": "https://www.jamieoliver.com/recipes/potato-recipes/balsamic-potatoes/",
    "source": "Jamie Oliver",
    "total_time": "2 hrs 20 mins",
    "ingredients":
    [
      "Balsamic vinegar",
      "maris piper potatoes",
      "red onions",
      "rocket",
      "olive oil"
    ],
    "thumbnail": "https://serpapi.com/searches/61c3369cc99903747c1e643b/images/bd928f9ef521c02bbdb2df96157011d6a02d619d06f8a6e0e62bb157f48e10e5.jpeg"

*/

// Project imports:
import 'package:digestableme/app/data/json_server_adpater.dart';
import 'package:digestableme/digests/recipes/model/ingredient.dart';
import 'package:digestableme/digests/recipes/model/step.dart';
import 'package:flutter_data/flutter_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'recipe.g.dart';

@JsonSerializable()
@DataRepository([JsonServerAdapter])
class Recipe extends DataModel<Recipe> {
  @override
  final int? id;
  final String title;
  final String recipeLink;
  final String author;
  final String duration;
  final HasMany<Ingredient> ingredients;
  final HasMany<Step> steps;
  final String thumbnail;

  Recipe({
    this.id,
    required this.title,
    required this.recipeLink,
    required this.author,
    required this.duration,
    required this.ingredients,
    required this.steps,
    required this.thumbnail,
  });

  static _mapSteps(dynamic jsonSteps) {
    HasMany<Step> steps = HasMany<Step>();
    for (var ingredient in jsonSteps) {
      steps.add(Step(
        sequenceNumber: int.parse(ingredient["sequenceNumber"]),
        instruction: ingredient["instruction"],
      ));
    }
    return steps;
  }

  static _mapIngredients(dynamic jsonIngredients) {
    HasMany<Ingredient> ingredients = HasMany<Ingredient>();
    for (var ingredient in jsonIngredients) {
      ingredients.add(Ingredient(
          ingredientType: ingredient["type"],
          quantityType: ingredient["quantityType"],
          name: ingredient["name"],
          quantity: int.parse(ingredient["quantity"])));
    }
    return ingredients;
  }

  static _mapSerpIngredients(dynamic jsonIngredients) {
    HasMany<Ingredient> ingredients = HasMany<Ingredient>();
    for (var ingredient in jsonIngredients) {
      ingredients.add(Ingredient(
        name: ingredient,
      ));
    }
    return ingredients;
  }

  static List<Recipe> fromResultJson(dynamic jsonResult) {
    var recipes = jsonResult;
    var result = recipes
        .map<Recipe>((recipe) => Recipe(
              title: recipe['title'],
              ingredients: _mapIngredients(recipe["ingredients"]).toList(),
              steps: _mapSteps(recipe["steps"]).toList(),
              recipeLink: recipe['origin'],
              author: recipe['author'],
              thumbnail: recipe['titleImage'],
              duration: "",
            ))
        .toList();
    return result;
  }

  static List<Recipe> fromSerpApiResultJson(dynamic jsonResult) {
    var recipes = jsonResult['recipes_results'];
    var result = recipes
        .map<Recipe>((recipe) => Recipe(
              title: recipe['title'],
              ingredients: _mapSerpIngredients(recipe["ingredients"]),
              steps: HasMany<Step>(),
              recipeLink: recipe['link'],
              author: recipe['source'],
              thumbnail: recipe['thumbnail'],
              duration: recipe['total_time'],
            ))
        .toList();
    return result;
  }
}
