// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ingredient.dart';

// **************************************************************************
// RepositoryGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, duplicate_ignore

mixin $IngredientLocalAdapter on LocalAdapter<Ingredient> {
  static final Map<String, RelationshipMeta> _kIngredientRelationshipMetas = {};

  @override
  Map<String, RelationshipMeta> get relationshipMetas =>
      _kIngredientRelationshipMetas;

  @override
  Ingredient deserialize(map) {
    map = transformDeserialize(map);
    return _$IngredientFromJson(map);
  }

  @override
  Map<String, dynamic> serialize(model, {bool withRelationships = true}) {
    final map = _$IngredientToJson(model);
    return transformSerialize(map, withRelationships: withRelationships);
  }
}

final _ingredientsFinders = <String, dynamic>{};

// ignore: must_be_immutable
class $IngredientHiveLocalAdapter = HiveLocalAdapter<Ingredient>
    with $IngredientLocalAdapter;

class $IngredientRemoteAdapter = RemoteAdapter<Ingredient> with NothingMixin;

final internalIngredientsRemoteAdapterProvider =
    Provider<RemoteAdapter<Ingredient>>((ref) => $IngredientRemoteAdapter(
        $IngredientHiveLocalAdapter(ref), InternalHolder(_ingredientsFinders)));

final ingredientsRepositoryProvider =
    Provider<Repository<Ingredient>>((ref) => Repository<Ingredient>(ref));

extension IngredientDataRepositoryX on Repository<Ingredient> {}

extension IngredientRelationshipGraphNodeX
    on RelationshipGraphNode<Ingredient> {}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Ingredient _$IngredientFromJson(Map<String, dynamic> json) => Ingredient(
      id: json['id'] as int?,
      name: json['name'] as String,
      quantityType: json['quantityType'] as String?,
      ingredientType: json['ingredientType'] as String?,
      quantity: json['quantity'] as int?,
    );

Map<String, dynamic> _$IngredientToJson(Ingredient instance) =>
    <String, dynamic>{
      'id': instance.id,
      'quantity': instance.quantity,
      'quantityType': instance.quantityType,
      'name': instance.name,
      'ingredientType': instance.ingredientType,
    };
