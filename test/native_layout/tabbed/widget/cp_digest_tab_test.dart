// ignore_for_file: deprecated_member_use

/*
  CpTab - Well polished useful widget, brilliantly coded with care.
*/
// #     #                                 #######
// #  #  # # #####   ####  ###### #####       #    ######  ####  #####
// #  #  # # #    # #    # #        #         #    #      #        #
// #  #  # # #    # #      #####    #         #    #####   ####    #
// #  #  # # #    # #  ### #        #         #    #           #   #
// #  #  # # #    # #    # #        #         #    #      #    #   #
//  ## ##  # #####   ####  ######   #         #    ######  ####    #

// Flutter imports:
import 'package:digestableme/digests/recipes/widget/android_digest_tab.dart';
import 'package:digestableme/digests/recipes/widget/cp_digest_tab.dart';
import 'package:digestableme/digests/recipes/widget/ios_digests_tab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simbucore/app/core/model/application_mode.dart';
import 'package:universal_platform/universal_platform.dart';

// Project imports:
import 'package:simbucore/app/core/provider/application_mode_provider.dart';
import 'package:simbucore/app/core/provider/platform_provider.dart';
import '../../../scaffold/finder.dart';

//  ######
// #     # #    # #    #
// #     # #    # ##   #
// ######  #    # # #  #
// #   #   #    # #  # #
// #    #  #    # #   ##
// #     #  ####  #    #

void main() {
  group('CpTab - area: ', () {
    cupertinoDigestTabForIosDevices();
    materialDigestTabForAndroidDevices();
  });
}

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

Widget buildWidget(UniversalPlatformType platformType) {
  Widget widget = const CupertinoApp(
    home: CpDigestTab(),
  );

  if (platformType == UniversalPlatformType.Android) {
    widget = const MaterialApp(
      home: Material(
        child: CpDigestTab(),
      ),
    );
  }

  return ProviderScope(
    overrides: [
      applicationModeProvider
            .overrideWithProvider(StateProvider<ApplicationMode>(
          (ref) => ApplicationMode.testing,
        )),
      platformProvider.overrideWithValue(platformType)
    ],
    child: widget,
  );
}

Future<void> cupertinoDigestTabForIosDevices() async {
  return testWidgets('Displays a Cupertino Digest Tab for iPhones',
      (WidgetTester tester) async {
    await tester.pumpWidget(Builder(builder: (BuildContext context) {
      return buildWidget(UniversalPlatformType.IOS);
    }));
    isPresent<IosDigestsTab>();
  });
}

Future<void> materialDigestTabForAndroidDevices() async {
  return testWidgets('Displays a Material Digest Tab for Android phones',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      buildWidget(UniversalPlatformType.Android),
    );
    isPresent<AndroidDigestTab>();
  });
}
