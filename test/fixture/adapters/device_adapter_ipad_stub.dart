/*
  Device Adapter Stub
 */

// Project imports:
import 'package:simbucore/app/core/model/device.dart';
import 'package:simbucore/app/core/service/device_adapter.dart';
import '../../app/values/model/fixture/device_fixture.dart';

class DeviceAdapterIpadStub implements DeviceAdapter{
  // Use the current device info to 
  @override
  Future<Device> getDevice() async {
    return DeviceFixtureIPad.buildDevice();
  }   
}
