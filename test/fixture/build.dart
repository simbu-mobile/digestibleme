// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:layout_lego/models/dl_tab_item.dart';

List<DlTabItem> buildTabItems(int number) {
  var tabItems = <DlTabItem>[];
  for (var i = 0; i < number; i++) {
    tabItems.add(DlTabItem(
      label: "label$i",
      icon: const Icon(CupertinoIcons.group),
      content: Container(
        key: Key("TabbedLayoutContentArea$i"),
        color: CupertinoColors.activeBlue,
      ),
      globalKey: GlobalKey<NavigatorState>(),
      navigationBar: null,
    ));
  }
  return tabItems;
}
