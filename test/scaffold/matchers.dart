// Package imports:
import 'package:test/test.dart';

/*
  Usage:

  expect(
    () => Version.increment(version, level).toString(),
    throwsAssertionWith("Do not increment"),
  );
*/
Matcher throwsAssertionWith(String messageSubString) {
  return throwsA(
      isA<AssertionError>().having(
          (AssertionError e) => e.toString(),
          'description',
          contains(messageSubString),
      ),
  );
}

Matcher throwsExceptionWith(String messageSubString) {
  return throwsA(
      isA<Exception>().having(
          (Exception e) => e.toString(),
          'description',
          contains(messageSubString),
      ),
  );
}
