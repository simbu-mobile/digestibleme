/*
  Gets location details to aid the experimental position tests. 

  Do we need to write tests like this or just rely on a 
  visual tester/sponsor confirmation ?

  Usage:
  Future<void> titleIsPositionedToTheRightOfTheImage() async {
    return testWidgets('Title is right of the Image',
        (WidgetTester tester) async {
      await tester.pumpWidget(buildListCard());
      var titleLocation = Location.getOffset(find.text("Joe Buck").first);
      var imageLocation = Location.getOffset(find.byType(CircleAvatar).first);
      expect(titleLocation.dx, greaterThan(imageLocation.dx));
    });
  }
 */

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

class Location{
  static Offset _getLocation(Element element) {
    return (element.findRenderObject() as RenderBox).localToGlobal(Offset.zero);
  }

  static Offset getOffset(Finder finder){
    return _getLocation(finder.evaluate().first);
  }
}
