// Flutter imports:
import 'package:flutter/widgets.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

T findFirstDescendent<T extends Widget>(WidgetTester tester, Type parentType) =>
    tester.widget<T>(find
        .descendant(of: find.byType(parentType), matching: find.byType(T))
        .first);

void isAbsent<T>() {
  expect(find.byType(T), findsNothing);
}

void isPresent<T>() {
  expect(find.byType(T), findsOneWidget);
}