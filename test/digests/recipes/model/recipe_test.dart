/*
  Recipe - Deserialie from json tests.
*/

// Dart imports:
import 'dart:core';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:digestableme/app/data/api_client.dart';
import 'package:digestableme/digests/recipes/model/recipe.dart';

//
// #     #                   #######
// #     # #    # # #####       #    ######  ####  #####  ####
// #     # ##   # #   #         #    #      #        #   #
// #     # # #  # #   #         #    #####   ####    #    ####
// #     # #  # # #   #         #    #           #   #        #
// #     # #   ## #   #         #    #      #    #   #   #    #
//  #####  #    # #   #         #    ######  ####    #    ####
//

//  ######
// #     # #    # #    #
// #     # #    # ##   #
// ######  #    # # #  #
// #   #   #    # #  # #
// #    #  #    # #   ##
// #     #  ####  #    #

void main() {
  group('Recipe, Deserialise API result: ', () {
    mapsRecipes();
    mapsTitle();
    mapsLink();
    mapsSource();
    mapsThumbnail();
    mapsIngredients();
    mapsSteps();
  });

  group('Recipe, Deserialise SerpAPI result: ', () {
    serpMapsRecipes();
    serpMapsTitle();
    serpMapsLink();
    serpMapsSource();
    serpMapsTotalTime();
    serpMapsThumbnail();
    serpMapsIngredients();
  });
}

/*
######                                                                    #    ######  ### 
#     # ######  ####  ###### #####  #   ##   #      #  ####  ######      # #   #     #  #  
#     # #      #      #      #    # #  #  #  #      # #      #          #   #  #     #  #  
#     # #####   ####  #####  #    # # #    # #      #  ####  #####     #     # ######   #  
#     # #           # #      #####  # ###### #      #      # #         ####### #        #  
#     # #      #    # #      #   #  # #    # #      # #    # #         #     # #        #  
######  ######  ####  ###### #    # # #    # ###### #  ####  ######    #     # #       ### 
*/

Future<List<Recipe>> mappedRecipes() async {
  final recipesData = await ApiClient().get("/recipe");
  var recipes = Recipe.fromResultJson(recipesData.data["Recipies"]).toList();
  return recipes;
}
Future<void> mapsRecipes() async {
  return test("Maps recipes",
      () {
    mappedRecipes().then((recipes) => expect(recipes.length,1));
  });
}

Future<void> mapsTitle() async {
  return test('Maps recipe title', () {
    mappedRecipes().then((recipes) => expect(recipes[0].title, "How to boil an egg"));
  });
}

Future<void> mapsLink() async {
  return test('Maps recipe link', () {
    mappedRecipes().then((recipes) => expect(recipes[0].recipeLink, "https://www.theguardian.com/lifeandstyle/2014/nov/11/how-to-boil-an-egg-the-heston-blumenthal-way"));
  });
}

Future<void> mapsSource() async {
  return test('Maps recipe source', () {
    mappedRecipes().then((recipes) => expect(recipes[0].author, "Heston Blumenthal"));
  });
}

Future<void> mapsThumbnail() async {
  
return test('Maps recipe thumbnail', () {
    mappedRecipes().then((recipes) => expect(recipes[0].thumbnail, "https://i.guim.co.uk/img/static/sys-images/Guardian/Pix/pictures/2014/11/5/1415205733799/4bfbd71a-6cd0-4494-833f-eaaed20a15b3-1020x612.jpeg?width=620&quality=45&auto=format&fit=max&dpr=2&s=ca3a95d7e761d267eff1b79b58cc4849"));
  });
}

Future<void> mapsIngredients() async {
  return test('Maps recipe ingredients', () {
    mappedRecipes().then((recipes) => expect(recipes[0].ingredients.length, 1));
  });
}

Future<void> mapsSteps() async {
  return test('Maps recipe instructions', () {
    mappedRecipes().then((recipes) => expect(recipes[0].steps.length, 3));
  });
}

/*
######                                                                  #####                          #    ######  ### 
#     # ######  ####  ###### #####  #   ##   #      #  ####  ######    #     # ###### #####  #####    # #   #     #  #  
#     # #      #      #      #    # #  #  #  #      # #      #         #       #      #    # #    #  #   #  #     #  #  
#     # #####   ####  #####  #    # # #    # #      #  ####  #####      #####  #####  #    # #    # #     # ######   #  
#     # #           # #      #####  # ###### #      #      # #               # #      #####  #####  ####### #        #  
#     # #      #    # #      #   #  # #    # #      # #    # #         #     # #      #   #  #      #     # #        #  
######  ######  ####  ###### #    # # #    # ###### #  ####  ######     #####  ###### #    # #      #     # #       ### 
*/

Future<List<Recipe>> serpMappedRecipes() async {
  final recipesData = await ApiClient().get("/recipe/search?q=Roast+potatoes+with+balsamic");
  var recipes = Recipe.fromSerpApiResultJson(recipesData.data).toList();
  return recipes;
}

Future<void> serpMapsRecipes() async {
  return test('Maps recipes', () {
    mappedRecipes().then((recipes) => expect(recipes.length,2));
  });
}

Future<void> serpMapsTitle() async {
  return test('Maps recipe title', () {
    mappedRecipes().then((recipes) => expect(recipes[0].title, "Balsamic potatoes"));
  });
}

Future<void> serpMapsLink() async {
  return test('Maps recipe link', () {
    mappedRecipes().then((recipes) => expect(recipes[0].recipeLink, "https://www.jamieoliver.com/recipes/potato-recipes/balsamic-potatoes/"));
  });
}

Future<void> serpMapsSource() async {
  return test('Maps recipe source', () {
    mappedRecipes().then((recipes) => expect(recipes[0].author, "Jamie Oliver"));
  });
}

Future<void> serpMapsTotalTime() async {
  return test('Maps recipe total time', () {
    mappedRecipes().then((recipes) => expect(recipes[0].duration, "2 hrs 20 mins"));
  });
}

Future<void> serpMapsThumbnail() async {
  return test('Maps recipe thumbnail', () {
    mappedRecipes().then((recipes) => expect(recipes[0].thumbnail, "assets/balsamicpotatoes_jamie.jpg"));
  });
}

Future<void> serpMapsIngredients() async {
  return test('Maps recipe ingredients', () {
    mappedRecipes().then((recipes) => expect(recipes[0].ingredients.length, 5));
  });
}
