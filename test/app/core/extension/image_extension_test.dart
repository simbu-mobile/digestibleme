/*
  NetWorkImage - Well polished useful widget, brilliantly coded with care.
*/
// #     #                                 #######
// #  #  # # #####   ####  ###### #####       #    ######  ####  #####
// #  #  # # #    # #    # #        #         #    #      #        #
// #  #  # # #    # #      #####    #         #    #####   ####    #
// #  #  # # #    # #  ### #        #         #    #           #   #
// #  #  # # #    # #    # #        #         #    #      #    #   #
//  ## ##  # #####   ####  ######   #         #    ######  ####    #

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:simbucore/app/core/model/application_mode.dart';

// Project imports:
import 'package:simbucore/app/core/extension/image_extension.dart';
import 'package:asset_lego/dl_avatar.dart';


//  ######
// #     # #    # #    #
// #     # #    # ##   #
// ######  #    # # #  #
// #   #   #    # #  # #
// #    #  #    # #   ##
// #     #  ####  #    #

void main() {
  group('NetWorkImage', () {
    stubsHttpRequestWhenAppModeIsWidgetTest();
    makesHttpRequestWhenAppModeIsNotWidgetTest();
  });
}

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

Widget buildWidget(BuildContext context, ApplicationMode applicationMode) {
  var widget = MaterialApp(
    home: Builder(
        builder: (BuildContext context) => DlAvatar(context.networkImage(
              "https://www.google.com",
              applicationMode,
            ))),
  );
  return widget;
}

Future<void> stubsHttpRequestWhenAppModeIsWidgetTest() async {
  return testWidgets(
      'Stubs out the http request for net images when running a widget test, just returns a transparent image',
      (WidgetTester tester) async {
    await tester.pumpWidget(Builder(builder: (BuildContext context) {
      return buildWidget(context, ApplicationMode.testing);
    }));

    final DlAvatar avatar =
        find.byType(DlAvatar).evaluate().single.widget as DlAvatar;
    expect(avatar.image.image is MemoryImage, true);
  });
}

Future<void> makesHttpRequestWhenAppModeIsNotWidgetTest() async {
  return testWidgets(
      'Makes the http request for net images when not running a widget test',
      (WidgetTester tester) async {
    await tester.pumpWidget(Builder(builder: (BuildContext context) {
      return buildWidget(context, ApplicationMode.running);
    }));

    expect(tester.takeException(), isInstanceOf<NetworkImageLoadException>());
  });
}
