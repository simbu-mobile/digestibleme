// Project imports:
import 'package:simbucore/app/core/model/device.dart';

class DeviceFixtureIPad{
  static const String name = "iPad Pro (11-inch) (3rd generation)";
  static const String model = "iPad";
  static const String version = "15.2";
  static const String identifier = "F248AEEE-E3F9-497B-829F-21C679993428";

  static Device buildDevice() => Device(name: name, version: version, identifier: identifier, model: model);
}

class DeviceFixtureIPhone{
  static const String name = "iPhone 13 Pro";
  static const String model = "iPhone";
  static const String version = "15.2";
  static const String identifier = "16BE509F-4CC0-460F-9AD2-A8C3CC3F6DB8";

  static Device buildDevice() => Device(name: name, version: version, identifier: identifier, model: model);
}
