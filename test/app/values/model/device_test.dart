/*
  Device Tests -  Used to get information about the current client device.
*/

// Dart imports:
import 'dart:core';

// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'fixture/device_fixture.dart';

// Project imports:

void main() {
  group('A Client Device: ', () {
    hasADeviceName();
    knowsTheDeviceVersion();
    knowsTheDeviceId();
    isIpad();
  });
}

Future<void> hasADeviceName() async {
  return test("Knows the device name like 'iPhone'.", () {
    expect(DeviceFixtureIPad.buildDevice().name, DeviceFixtureIPad.name);
  });
}

Future<void> knowsTheDeviceVersion() async {
  return test("Knows the device version e.g. 13.1", () {
    expect(DeviceFixtureIPad.buildDevice().version, DeviceFixtureIPad.version);
  });
}

Future<void> knowsTheDeviceId() async {
  return test("Knows the device unqiue id (UUID) e.g. xxxx-xxxx", () {
    expect(DeviceFixtureIPad.buildDevice().identifier, DeviceFixtureIPad.identifier);
  });
}

Future<void> isIpad() async {
  return test("knows if its an iPad.", () {
    expect(DeviceFixtureIPad.buildDevice().isIPad, true);
  });
}
