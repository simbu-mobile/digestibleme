/*
   A function to adds new values with a count of 1 or increment the counter when you attempt to add the same value a 2nd time.
*/

// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:simbucore/app/event_store/extension/counter_functions.dart';

//
// #     #                   ####### 
// #     # #    # # #####       #    ######  ####  ##### 
// #     # ##   # #   #         #    #      #        #   
// #     # # #  # #   #         #    #####   ####    #   
// #     # #  # # #   #         #    #           #   #   
// #     # #   ## #   #         #    #      #    #   #   
//  #####  #    # #   #         #    ######  ####    #   
//


// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

void main() {
    group('Function - Add new, increment existing counts in a map: ', () {
        newValuesHaveACountOfOne();
        existingValuesAreIncremented();
        existingValuesCanBeIncrementedMoreThanOnce();
    });
}

Future<void> newValuesHaveACountOfOne() async {
   return test('New values have a count of one', () {
      const valueLabel = "Value1";
      var countMap = <String, int>{};
      countMap = addOrIncrementValue(countMap, valueLabel);
      expect(countMap[valueLabel], 1);
   });
}

Future<void> existingValuesAreIncremented() async {
   return test('Existing values are incremented', () {
      const valueLabel = "Value1";
      var countMap = <String, int>{};
      countMap = addOrIncrementValue(countMap, valueLabel);
      countMap = addOrIncrementValue(countMap, valueLabel);
      expect(countMap[valueLabel], 2);
   });
}

Future<void> existingValuesCanBeIncrementedMoreThanOnce() async {
   return test('Existing values can be incremented more than once', () {
      const valueLabel = "Value1";
      var countMap = <String, int>{};
      countMap = addOrIncrementValue(countMap, valueLabel);
      countMap = addOrIncrementValue(countMap, valueLabel);
      countMap = addOrIncrementValue(countMap, valueLabel);
      expect(countMap[valueLabel], 3);
   });
}
